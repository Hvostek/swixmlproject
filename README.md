### What is this repository for? ###
This repository for project wich include SwiXML+Jgoodie libraries.
### How do I get set up? ###
1. Clone the Git;
2. Setup the properties of your IDE(src,lib, e.t.c);
3. Run the Form.java
### What should you see after the launch? ###
![picture](/img/example.jpg)
### Useful links ###
* [SWIXML](https://wolfpaulus.com/software/swixml/)
* [JGoodie FormLayout](http://manual.openestate.org/extern/forms-1.2.1/tutorial/quickstart.html)
