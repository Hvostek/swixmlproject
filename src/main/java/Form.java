package main.java;

import com.toedter.calendar.JCalendar;
import org.swixml.SwingEngine;

import javax.swing.*;
import javax.swing.plaf.synth.SynthLookAndFeel;
import java.awt.event.ActionEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;


/**
 * The Form class shows how to do a simple JGoodies FormLayout
 */
public class Form extends SwingEngine {

    private static final String ABOUT = "Example of code for SwiXml \nVersion: 0.01";
    private static final String SYNTH_LOOK_AND_FEEL_CONFIG_FILE = "src/main/java/xml/TheSynthLookandFeelSettings.xml";
    private static final String RENDER_FILE = "main/java/xml/form.xml";

    private JTextArea jTextArea;
    private JButton jButton;
    private JTextField name;
    private JTextField sureName;
    private JTextField company;
    private JComboBox sex;
    private JComboBox position;
    private JCheckBox pet;
    private JCalendar dateOfBirthday;
    private JFileChooser jFileChooser;


    public Form() {
        try {
            initLookAndFeel();
            JFrame.setDefaultLookAndFeelDecorated(true);
            //the code for custom tag
            getTaglib().registerTag("Calendar", JCalendar.class);
            render(RENDER_FILE).setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void initLookAndFeel() {
        SynthLookAndFeel lookAndFeel = new SynthLookAndFeel();
        try {
            lookAndFeel.load(new URL("file:///" + new File(SYNTH_LOOK_AND_FEEL_CONFIG_FILE).getAbsolutePath()));
            UIManager.setLookAndFeel(lookAndFeel);
        } catch (Exception e) {
            System.err.println("Couldn't get specified look and feel ("
                    + lookAndFeel
                    + "), for some reason.");
            System.err.println("Using the default look and feel.");
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        new Form();
    }

    public Action showInfo = new AbstractAction() {
        public void actionPerformed(ActionEvent e) {
            jTextArea.setText(
                            "Name: " + name.getText() + "\n" +
                            "Surename: " + sureName.getText() + "\n" +
                            "Company Name: " + company.getText() + "\n" +
                            "Sex: " + sex.getSelectedItem() + "\n" +
                            "Birthday: " + name.getText() + " birthday on: " + dateOfBirthday.getDate().toString() + "\n" +
                            "Position: " + position.getSelectedItem() + "\n" +
                            "Pet: " + name.getText() + Form.petCheck(pet));
        }
    };

    public Action aboutAction = new AbstractAction() {
        public void actionPerformed(ActionEvent e) {
            JOptionPane.showMessageDialog(getRootComponent(), ABOUT, "About", JOptionPane.INFORMATION_MESSAGE);
        }
    };

    public Action exitAction = new AbstractAction() {
        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    };

    private static String petCheck(JCheckBox jCheckBox) {
        if (jCheckBox.isSelected())
            return " has a pet! That's great!";
        else {
            return " hasn't pet :(";
        }
    }

    public Action saveFile = new AbstractAction() {
        public void actionPerformed(ActionEvent e) {
            JFileChooser saver = new JFileChooser("./");
            int returnVal = saver.showSaveDialog(Form.getAppFrame());
            File file = saver.getSelectedFile();
            BufferedWriter writer = null;
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                try {
                    writer = new BufferedWriter(new FileWriter(file.getAbsolutePath()));
                    writer.write(jTextArea.getText());
                    writer.close();
                    JOptionPane.showMessageDialog(Form.getAppFrame(), "The text was Saved Successfully!",
                            "Success!", JOptionPane.INFORMATION_MESSAGE);
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(Form.getAppFrame(), "The Text could not be Saved!",
                            "Error!", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        }
    };
}